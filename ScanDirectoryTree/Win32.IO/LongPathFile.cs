﻿using Microsoft.Win32.SafeHandles;
using ScanDirectoryTree.Interop;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace ScanDirectoryTree.Win32.IO
{
    /// <summary>
    ///     Provides static methods for creating, copying, deleting, moving, and opening of files
    ///     with long paths, that is, paths that exceed 259 characters.
    /// </summary>
    public static class LongPathFile
    {

        /// <summary>
        ///     Returns a value indicating whether the specified path refers to an existing file.
        /// </summary>
        /// <param name="path">
        ///     A <see cref="String"/> containing the path to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if <paramref name="path"/> refers to an existing file; 
        ///     otherwise, <see langword="false"/>.
        /// </returns>
        /// <remarks>
        ///     Note that this method will return false if any error occurs while trying to determine 
        ///     if the specified file exists. This includes situations that would normally result in 
        ///     thrown exceptions including (but not limited to); passing in a file name with invalid 
        ///     or too many characters, an I/O error such as a failing or missing disk, or if the caller
        ///     does not have Windows or Code Access Security (CAS) permissions to to read the file.
        /// </remarks>
        public static bool Exists(string path)
        {

            bool isDirectory;
            if (LongPathCommon.Exists(path, out isDirectory))
            {

                return !isDirectory;
            }

            return false;
        }



        private static SafeFindHandle BeginFind(string normalizedPathWithSearchPattern, out NativeMethods.WIN32_FIND_DATA findData)
        {

            SafeFindHandle handle = NativeMethods.FindFirstFile(normalizedPathWithSearchPattern, out findData);
            if (handle.IsInvalid)
            {

                int errorCode = Marshal.GetLastWin32Error();
                if (errorCode != NativeMethods.ERROR_FILE_NOT_FOUND)
                    throw LongPathCommon.GetExceptionFromWin32Error(errorCode);

                return null;
            }

            return handle;
        }

        public static int Size(string path)
        {
            string normalizedPath = LongPathCommon.NormalizeLongPath(path);
            NativeMethods.WIN32_FIND_DATA findData;
            using (SafeFindHandle handle = BeginFind(normalizedPath, out findData))
            {
                if (handle == null)
                    return 0;
                return ((findData.nFileSizeHigh * (2 ^ 32)) + findData.nFileSizeLow);

            }
        }

        public static FileAttributes GetFileAttributes(string path)
        {
            string normalizedPath = LongPathCommon.NormalizeLongPath(path);
            NativeMethods.WIN32_FIND_DATA findData;

            using (SafeFindHandle handle = BeginFind(normalizedPath, out findData))
            {
                if (handle == null)
                    return (FileAttributes)NativeMethods.INVALID_FILE_ATTRIBUTES;
                return findData.dwFileAttributes;

            }
        }

        public static DateTime CreatedOn(string path)
        {
            string normalizedPath = LongPathCommon.NormalizeLongPath(path);
            NativeMethods.WIN32_FIND_DATA findData;
            using (SafeFindHandle handle = BeginFind(normalizedPath, out findData))
            {
                if (handle == null)
                    return DateTime.MinValue;
                var ft = findData.ftCreationTime;
                long hFT2 = (((long)ft.dwHighDateTime) << 32) + ft.dwLowDateTime;
                return DateTime.FromFileTime(hFT2);
            }
        }

        public static DateTime ModifiedOn(string path)
        {
            string normalizedPath = LongPathCommon.NormalizeLongPath(path);
            NativeMethods.WIN32_FIND_DATA findData;
            using (SafeFindHandle handle = BeginFind(normalizedPath, out findData))
            {
                if (handle == null)
                    return DateTime.MinValue;
                var ft = findData.ftLastWriteTime;
                long hFT2 = (((long)ft.dwHighDateTime) << 32) + ft.dwLowDateTime;
                return DateTime.FromFileTime(hFT2);
            }
        }

        public static DateTime LastAccessDate(string path)
        {
            string normalizedPath = LongPathCommon.NormalizeLongPath(path);
            NativeMethods.WIN32_FIND_DATA findData;
            using (SafeFindHandle handle = BeginFind(normalizedPath, out findData))
            {
                if (handle == null)
                    return DateTime.MinValue;
                var ft = findData.ftLastAccessTime;
                long hFT2 = (((long)ft.dwHighDateTime) << 32) + ft.dwLowDateTime;
                return DateTime.FromFileTime(hFT2);
            }
        }

        public static FileSecurity GetAccessControl(String path)
        {
            const AccessControlSections includeSections = AccessControlSections.Access | AccessControlSections.Owner | AccessControlSections.Group;
            return GetAccessControl(path, includeSections);
        }

        public static FileSecurity GetAccessControl(String path, AccessControlSections includeSections)
        {
            var normalizedPath = LongPathCommon.NormalizeLongPath(path);
            IntPtr sidOwner, sidGroup, dacl, sacl, byteArray;
            var securityInfos = LongPathCommon.ToSecurityInfos(includeSections);

            var errorCode = (int)NativeMethods.GetSecurityInfoByName(normalizedPath,
                (int)ResourceType.FileObject,
                (int)securityInfos,
                out sidOwner,
                out sidGroup,
                out dacl,
                out sacl,
                out byteArray);

            if (errorCode == NativeMethods.ERROR_ACCESS_DENIED)
            {
                throw LongPathCommon.GetExceptionFromWin32Error(errorCode);
            }

            var length = NativeMethods.GetSecurityDescriptorLength(byteArray);

            var binaryForm = new byte[length];

            Marshal.Copy(byteArray, binaryForm, 0, (int)length);

            NativeMethods.LocalFree(byteArray);
            var ds = new FileSecurity();
            ds.SetSecurityDescriptorBinaryForm(binaryForm);
            return ds;
        }
    }
}
