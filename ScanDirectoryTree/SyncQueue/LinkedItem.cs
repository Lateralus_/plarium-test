﻿namespace ScanDirectoryTree.SyncQueue
{
    public class LinkedItem<T>
    {
        public LinkedItem(T item)
        {
            Value = item;
        }

        public T Value { get; private set; }
        public LinkedItem<T> Next { get; set; }
        public LinkedItem<T> Prev { get; set; }
    }
}

