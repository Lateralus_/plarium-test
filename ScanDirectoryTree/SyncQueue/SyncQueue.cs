﻿using System.Collections.Generic;
using System.Threading;

namespace ScanDirectoryTree.SyncQueue
{
    public class SyncQueue<T>
    {
        #region private variables
        private static readonly object SyncRoot = new object();
        private readonly Dictionary<LinkedItem<T>, int> _queue;
        private readonly int _consumersCount;
        private LinkedItem<T> _prev;
        #endregion

        #region public properties
        public ManualResetEventSlim RootAwaiter { get; private set; }

        public LinkedItem<T> Root { get; set; }

        public int Unconsumed
        {
            get
            {
                lock (SyncRoot)
                {
                    return _queue.Count;
                }
            }
        }
        #endregion

        public SyncQueue(int consumersCount)
        {
            _consumersCount = consumersCount;
            _queue = new Dictionary<LinkedItem<T>, int>();
            RootAwaiter = new ManualResetEventSlim();
        }

        #region public methods
        /// <summary>
        /// Adding item to queue with default number of it's consumers = 0
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            lock (SyncRoot)
            {
                var node = new LinkedItem<T>(item);

                _queue.Add(node, 0);

                if (Root == null)
                {
                    Root = node;
                    RootAwaiter.Set();
                }

                if (_prev != null)
                {
                    _prev.Next = node;
                    node.Prev = _prev;
                }

                _prev = node;
            }
        }

        /// <summary>
        /// Delete all items from queue
        /// </summary>
        public void Reset()
        {
            Root = null;
            _prev = null;
            _queue.Clear();
            RootAwaiter.Reset();
        }
        /// <summary>
        /// Remove item from the queue if all other consumers have already released it. Otherwise, increment releasing consumers count for the item.
        /// </summary>
        /// <param name="item">Queue item</param>
        public void Free(LinkedItem<T> item)
        {
            lock (SyncRoot)
            {
                if (_queue.ContainsKey(item))
                {
                    if (_queue[item] == _consumersCount - 1)
                    {
                        _queue.Remove(item);

                        if (item.Next != null && !_queue.ContainsKey(item.Next))
                            item.Next = null;

                        if (item.Prev != null && !_queue.ContainsKey(item.Prev))
                        {
                            item.Prev.Next = null;
                            item.Prev = null;
                        }
                    }
                    else
                    {
                        ++_queue[item];
                    }
                }
            }
        }
        #endregion
    }
}
