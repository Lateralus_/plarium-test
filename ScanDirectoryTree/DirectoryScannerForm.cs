﻿using System;
using System.IO;
using System.Windows.Forms;
using ScanDirectoryTree.Properties;
using ScanDirectoryTree.Parameters;
using ScanDirectoryTree.Win32.IO;

namespace ScanDirectoryTree
{
    public partial class DirectoryScannerForm : Form
    {
        /// <summary>
        /// Object containg main logic for current application
        /// </summary>
        private Executer _executer;
        /// <summary>
        /// Logging object. Handles status updates, warnings and errors showing to user 
        /// </summary>
        private Logger _logger;

        public DirectoryScannerForm()
        {
            InitializeComponent();

            _logger = new Logger(this.logListBox, this.readingStatusLabel,
                this.treeBuilderStatusLevel,
                this.xmlStatusLabel);

        }
        /// <summary>
        /// Ensure that threads will stop too
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirectoryTree_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_executer != null) _executer.Dispose();
        }
        /// <summary>
        /// Validate and save root directory path
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pathButton_Click(object sender, EventArgs e)
        {
            if (pathBrowsingDialog.ShowDialog() == DialogResult.OK)
            {
                string rootDirectoryPath = pathBrowsingDialog.SelectedPath;

                if (string.IsNullOrEmpty(rootDirectoryPath) || !LongPathDirectory.Exists(rootDirectoryPath))
                {
                    MessageBox.Show(Resources.DirectoryPathError_Body, Resources.DirectoryPathError_Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.rootDirectoryTextBox.Text = rootDirectoryPath;

                this.startScanButton.Enabled = !string.IsNullOrEmpty(this.outputXmlTextBox.Text) && !string.IsNullOrEmpty(rootDirectoryPath);
            }
        }
        /// <summary>
        /// Validate and save output file path for Xml saving
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void outputButton_Click(object sender, EventArgs e)
        {
            if (xmlOutputSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string outputFileName = xmlOutputSaveFileDialog.FileName;

                if (string.IsNullOrEmpty(outputFileName) || Path.GetExtension(outputFileName) != ".xml")
                {
                    MessageBox.Show(this, Resources.OutputFileSelectionError_Body, Resources.OutputFileSelectionError_Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.outputXmlTextBox.Text = outputFileName;

                this.startScanButton.Enabled = !string.IsNullOrEmpty(outputFileName) && !string.IsNullOrEmpty(this.rootDirectoryTextBox.Text);
            }
        }
        /// <summary>
        /// Instantiates _executer. Transfers further actions to this object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startScanButton_Click(object sender, EventArgs e)
        {
            this.startScanButton.Enabled = false;
            ExecuteParameter executeParameter = new ExecuteParameter
            {
                Logger = _logger,
                InfoTreeView = this.directoryTreeView,
                OutPutFileName = this.outputXmlTextBox.Text,
                Path = this.rootDirectoryTextBox.Text
            };
            _executer = new Executer(executeParameter);
        }
    }
}
