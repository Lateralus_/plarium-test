﻿using ScanDirectoryTree.Model;
using ScanDirectoryTree.Parameters;
using ScanDirectoryTree.SyncQueue;
using ScanDirectoryTree.Win32.IO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace ScanDirectoryTree
{
    public class Executer : IDisposable
    {
        #region private variables and properties
        /// <summary>
        /// Queue for concurrent deliverance
        /// </summary>
        private readonly SyncQueue<InfoBase> _queue;

        private readonly CancellationTokenSource _shutdownTokenSource;

        private CancellationToken ShutdownToken
        {
            get { return _shutdownTokenSource.Token; }
        }

        private Logger _logger;

        private XmlDocument _doc;
        private XmlNode _currentParent;
        private XmlNode _root;
        private string _outPutFileName;

        private TreeView _infoTreeView;

        private Action<InfoBase> actionAddingToTree;
        #endregion

        #region collections
        /// <summary>
        /// Dictionary for TreeNode items. Provides performance advantage in contrast with .Find()
        /// Key = InfoBase.Id
        /// Value = TreeNode
        /// </summary>
        private Dictionary<Guid, TreeNode> _treeNodes = new Dictionary<Guid, TreeNode>();
        /// <summary>
        /// Dictionary for XmlNode items. Provides performance advantage in contrast with using XmlNode.SelectNodes(XPath expr)
        /// Key = InfoBase.Id
        /// Value = XmlNode
        /// </summary>
        private Dictionary<Guid, XmlNode> _xmlNodes = new Dictionary<Guid, XmlNode>();
        #endregion

        /// <summary>
        /// Global XmlSerializer for items of type "Model.DirectoryInfo"
        /// XmlSerializers produce dynamic assemblies each time they are instantiated, so single instance is more reasonable
        /// </summary>
        public static XmlSerializer DirectorySerializer;
        /// <summary>
        /// Global XmlSerializer for items of type "Model.FileInfo"
        /// XmlSerializers produce dynamic assemblies each time they are instantiated, so single instance is more reasonable
        /// </summary>
        public static XmlSerializer FileSerializer;


        #region events
        private readonly ManualResetEventSlim _parsingAwaiter;
        #endregion

        /// <summary>
        /// Constructor. Initialize all fields. Run cuncurrent processes 
        /// </summary>
        /// <param name="parameter">Object containing all neccessary information coming from DirectoryScannerForm</param>
        public Executer(ExecuteParameter parameter)
        {
            _logger = parameter.Logger;

            _parsingAwaiter = new ManualResetEventSlim();
            _shutdownTokenSource = new CancellationTokenSource();
            _infoTreeView = parameter.InfoTreeView;
            _outPutFileName = parameter.OutPutFileName;

            this.actionAddingToTree = AddNodeToTreeViewControl;

            DirectorySerializer = new XmlSerializer(typeof(Model.DirectoryInfo));
            FileSerializer = new XmlSerializer(typeof(Model.FileInfo));

            _doc = new XmlDocument();
            try
            {
                _doc.Load(_outPutFileName);
                _root = _doc.DocumentElement;
            }
            catch (XmlException e)
            {
                if (string.Compare(e.Message, "Root element is missing.") == 0)
                {
                    _root = _doc.AppendChild(_doc.CreateElement("root"));
                }
                else
                {
                    _logger.Error(string.Concat("Unable to work with Xml output file. Exception: ", e.Message));
                    return;
                }
            }

            _doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            _doc.DocumentElement.SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");

            _queue = new SyncQueue<InfoBase>(2);

            _parsingAwaiter.Set();

            Reading(parameter.Path);

            _logger.UpdateDrawStatus("In Progress", Color.Yellow);
            Task.Factory.StartNew(Draw);

            _logger.UpdateSaveStatus("In Progress", Color.Yellow);
            Task.Factory.StartNew(Save);
        }

        #region Reading
        /// <summary>
        /// Method which triggers async directory scanning process.
        /// </summary>
        /// <param name="path">Root directory path</param>
        private async void Reading(string path)
        {
            _logger.UpdateReadStatus("In Progress", Color.Yellow);

            await Task.Run(() => Read(new ReadParameter { Path = path, ParentId = Guid.Empty }));

            _parsingAwaiter.Reset();

            _logger.UpdateReadStatus("Complete", Color.Green);
        }
        /// <summary>
        /// Directory scanning process. Being called from Reading method
        /// </summary>
        /// <param name="parameters"></param>
        private void Read(object parameters)
        {
            ShutdownToken.ThrowIfCancellationRequested();

            ReadParameter param = (ReadParameter)parameters;

            string path = param.Path;
            Guid parentId = param.ParentId;

            Model.DirectoryInfo newDirInfo = new Model.DirectoryInfo(path);
            if (newDirInfo.Exceptions.Count > 0)
            {
                foreach (Exception ex in newDirInfo.Exceptions)
                {
                    _logger.Warning(string.Concat("Was unable to retrieve secure info for directory: ",
                        newDirInfo.Path, Environment.NewLine, "Exception Message: ", ex.Message));
                }
            }
            newDirInfo.ParentId = parentId;
            _queue.Add(newDirInfo);

            string[] subdirectoryEntries;
            try
            {
                subdirectoryEntries = Directory.GetDirectories(path);
                foreach (string subdirectory in subdirectoryEntries)
                {
                    ReadParameter subDirParams = new ReadParameter { Path = subdirectory, ParentId = newDirInfo.Id };
                    Read(subDirParams);
                }
            }
            catch (DirectoryNotFoundException)
            {
                _logger.Warning(string.Concat("Was unable to open file or directory: ", newDirInfo.Path));
                subdirectoryEntries = new string[0];
            }
            catch (UnauthorizedAccessException)
            {
                _logger.Warning(string.Concat("Current user doesn't have access to: ", newDirInfo.Path));
            }
            catch (PathTooLongException)
            {
                try
                {
                    foreach (string subdirectory in LongPathDirectory.EnumerateDirectories(path))
                    {
                        ReadParameter subDirParams = new ReadParameter { Path = subdirectory, ParentId = newDirInfo.Id };
                        Read(subDirParams);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Warning(string.Concat("Was unable to get sub directories at path: ", path, " Excpetion: ", ex.Message));
                    subdirectoryEntries = new string[0];
                }
            }
            catch (Exception e)
            {
                _logger.Warning(string.Concat("Unexpected error: ", e.Message));
            }

            IEnumerable<string> fileEntries = new string[0];

            try
            {
                fileEntries = Directory.EnumerateFiles(path);
            }
            catch (FileNotFoundException)
            {
                _logger.Warning(string.Concat("Files were not found at: ", path));

            }
            catch (UnauthorizedAccessException)
            {
                _logger.Warning(string.Concat("Current user doesn't have access to: ", path));
            }
            catch (PathTooLongException)
            {
                try
                {
                    fileEntries = LongPathDirectory.EnumerateFiles(path);
                }
                catch (Exception ex)
                {
                    _logger.Warning("Was unable to get files at: ", path, " Exception: ", ex.Message);
                }
            }
            catch (Exception e)
            {
                _logger.Warning(string.Concat("Unexpected error: ", e.Message));

            }

            foreach (string fileEnty in fileEntries)
            {
                Model.FileInfo newFile = new Model.FileInfo(fileEnty);
                if (newFile.Exceptions.Count > 0)
                {
                    foreach (Exception ex in newFile.Exceptions)
                    {
                        _logger.Warning(string.Concat("Was unable to retrieve secure info for file: ",
                            newFile.Path, Environment.NewLine, "Exception Message: ", ex.Message));
                    }
                }
                newFile.ParentId = newDirInfo.Id;
                _queue.Add(newFile);
            }
        }
        #endregion

        #region Drawing
        /// <summary>
        /// Visual system files tree builder. Starts async automatically in constructor
        /// </summary>
        private void Draw()
        {
            while (true)
            {
                _parsingAwaiter.Wait(ShutdownToken);
                _queue.RootAwaiter.Wait(ShutdownToken);

                var item = _queue.Root;
                if (_infoTreeView.InvokeRequired)
                {
                    _infoTreeView.Invoke(actionAddingToTree, item.Value);
                }
                else
                {
                    actionAddingToTree(item.Value);
                }
                _queue.Free(item);

                while (_queue.Unconsumed > 0 || _parsingAwaiter.IsSet)
                {
                    ShutdownToken.ThrowIfCancellationRequested();

                    if (item.Next != null)
                    {
                        item = item.Next;
                        if (_infoTreeView.InvokeRequired)
                        {
                            _infoTreeView.Invoke(actionAddingToTree, item.Value);
                        }
                        else
                        {
                            actionAddingToTree(item.Value);
                        }
                        _queue.Free(item);  // free item from queue
                    }
                }
                _logger.UpdateDrawStatus("Complete", Color.Green);

                break;
            }
        }
        /// <summary>
        /// Action "actionAddingToTree" body. Updates visual tree control with new item.
        /// </summary>
        /// <param name="info">New item to update</param>
        private void AddNodeToTreeViewControl(InfoBase info)
        {
            if (info.ParentId != Guid.Empty)
            {
                var parent = _treeNodes.ContainsKey(info.ParentId) ? _treeNodes[info.ParentId] : _treeNodes[Guid.Empty];
                var newNode = parent.Nodes.Add(info.Id.ToString(), info.Name);
                _treeNodes.Add(info.Id, newNode);
            }
            else
            {
                TreeNode parent = _infoTreeView.Nodes.Add(info.Id.ToString(), info.Name);
                _treeNodes.Add(Guid.Empty, parent);
            }
        }
        #endregion

        #region Saving
        /// <summary>
        /// Xml system files tree xml serialization process. Starts async automatically in constructor
        /// </summary>
        private void Save()
        {
            while (true)
            {
                _parsingAwaiter.Wait(ShutdownToken);

                _queue.RootAwaiter.Wait(ShutdownToken);

                var item = _queue.Root;
                this.SaveNode(item.Value);
                _queue.Free(item);

                while (_queue.Unconsumed > 0 || _parsingAwaiter.IsSet)
                {
                    ShutdownToken.ThrowIfCancellationRequested();

                    if (item.Next != null)
                    {
                        item = item.Next;
                        this.SaveNode(item.Value);
                        _queue.Free(item);
                    }
                }

                try
                {
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    XmlWriter finalWriter = XmlWriter.Create(_outPutFileName, settings);
                    _doc.Save(finalWriter);

                    _logger.UpdateSaveStatus("Complete", Color.Green);
                }
                catch (Exception e)
                {
                    if (e is XmlException)
                    {
                        while (e.InnerException != null)
                        {
                            e = e.InnerException;
                        }
                        _logger.Error(string.Concat("Was unable to save to .xml file. Details: ", e.Message));
                    }
                    else
                    {
                        _logger.Error(string.Concat("Unexpected error while saving .xml file. Details: ", e.Message));
                    }

                    _logger.UpdateSaveStatus("Completed in Error", Color.Red);
                }

                break;
            }
        }
        /// <summary>
        /// Serializes new item to Xml. Adds item to _xmlNodes dictionary.
        /// </summary>
        /// <param name="info"></param>
        private void SaveNode(InfoBase info)
        {
            _currentParent = _xmlNodes.ContainsKey(info.ParentId) ? _xmlNodes[info.ParentId] : _root;
            try
            {
                _xmlNodes.Add(info.Id, serializeAppend(_currentParent, info));
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                _logger.Error(string.Format("Was unable to perform serialization for {0}. Details: {1}", info.Name, e.Message));
            }
        }
        /// <summary>
        /// Inner method for SaveNode. Provides serialization and AppendChild action
        /// </summary>
        /// <param name="parentNode">Parent to which apply AppendChild operation</param>
        /// <param name="newChild">New child to append</param>
        /// <returns>New child as XmlNode</returns>
        private XmlNode serializeAppend(XmlNode parentNode, InfoBase newChild)
        {
            XPathNavigator nav = parentNode.CreateNavigator();
            using (var writer = nav.AppendChild())
            {
                writer.WriteWhitespace("");

                if (newChild is Model.DirectoryInfo)
                {
                    DirectorySerializer.Serialize(writer, newChild);
                }
                else if (newChild is Model.FileInfo)
                {
                    FileSerializer.Serialize(writer, newChild);
                }
            }
            return parentNode.LastChild;
        }
        #endregion

        public void Dispose()
        {
            _queue.Reset();
            _shutdownTokenSource.Cancel();
        }
    }
}
