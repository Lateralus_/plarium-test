﻿using System.Windows.Forms;

namespace ScanDirectoryTree.Parameters
{
  public class ExecuteParameter
  {
    public Logger Logger;
    public TreeView InfoTreeView;
    public string OutPutFileName;
    public string Path;
  }
}
