﻿using System;

namespace ScanDirectoryTree.Parameters
{
    public class ReadParameter
    {
        public string Path { get; set; }
        public Guid ParentId { get; set; }
    }
}
