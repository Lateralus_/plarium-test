﻿namespace ScanDirectoryTree
{
    partial class DirectoryScannerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startScanButton = new System.Windows.Forms.Button();
            this.directoryTreeView = new System.Windows.Forms.TreeView();
            this.pathBrowsingDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.pathButton = new System.Windows.Forms.Button();
            this.outputButton = new System.Windows.Forms.Button();
            this.xmlOutputSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.processLabel = new System.Windows.Forms.Label();
            this.rootDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.outputXmlTextBox = new System.Windows.Forms.TextBox();
            this.treePanel = new System.Windows.Forms.Panel();
            this.xmlStatusLabel = new System.Windows.Forms.Label();
            this.treeBuilderStatusLevel = new System.Windows.Forms.Label();
            this.readingStatusLabel = new System.Windows.Forms.Label();
            this.directoryTreeLabel = new System.Windows.Forms.Label();
            this.errorLogPanel = new System.Windows.Forms.Panel();
            this.logListBox = new System.Windows.Forms.ListBox();
            this.errorLogLabel = new System.Windows.Forms.Label();
            this.treeBuilderHeaderLabel = new System.Windows.Forms.Label();
            this.ReadingHeaderLabel = new System.Windows.Forms.Label();
            this.xmlHeaderLabel = new System.Windows.Forms.Label();
            this.treePanel.SuspendLayout();
            this.errorLogPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // startScanButton
            // 
            this.startScanButton.Enabled = false;
            this.startScanButton.Location = new System.Drawing.Point(781, 76);
            this.startScanButton.Name = "startScanButton";
            this.startScanButton.Size = new System.Drawing.Size(75, 23);
            this.startScanButton.TabIndex = 1;
            this.startScanButton.Text = "Start";
            this.startScanButton.UseVisualStyleBackColor = true;
            this.startScanButton.Click += new System.EventHandler(this.startScanButton_Click);
            // 
            // directoryTreeView
            // 
            this.directoryTreeView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.directoryTreeView.Location = new System.Drawing.Point(0, 44);
            this.directoryTreeView.Name = "directoryTreeView";
            this.directoryTreeView.Size = new System.Drawing.Size(500, 446);
            this.directoryTreeView.TabIndex = 2;
            // 
            // pathButton
            // 
            this.pathButton.Location = new System.Drawing.Point(12, 12);
            this.pathButton.Name = "pathButton";
            this.pathButton.Size = new System.Drawing.Size(145, 23);
            this.pathButton.TabIndex = 3;
            this.pathButton.Text = "Choose directory";
            this.pathButton.UseVisualStyleBackColor = true;
            this.pathButton.Click += new System.EventHandler(this.pathButton_Click);
            // 
            // outputButton
            // 
            this.outputButton.Location = new System.Drawing.Point(12, 41);
            this.outputButton.Name = "outputButton";
            this.outputButton.Size = new System.Drawing.Size(145, 23);
            this.outputButton.TabIndex = 4;
            this.outputButton.Text = "Choose output Xml file";
            this.outputButton.UseVisualStyleBackColor = true;
            this.outputButton.Click += new System.EventHandler(this.outputButton_Click);
            // 
            // processLabel
            // 
            this.processLabel.AutoSize = true;
            this.processLabel.Location = new System.Drawing.Point(12, 76);
            this.processLabel.Name = "processLabel";
            this.processLabel.Size = new System.Drawing.Size(0, 13);
            this.processLabel.TabIndex = 5;
            // 
            // rootDirectoryTextBox
            // 
            this.rootDirectoryTextBox.Location = new System.Drawing.Point(164, 14);
            this.rootDirectoryTextBox.Name = "rootDirectoryTextBox";
            this.rootDirectoryTextBox.ReadOnly = true;
            this.rootDirectoryTextBox.Size = new System.Drawing.Size(692, 20);
            this.rootDirectoryTextBox.TabIndex = 6;
            // 
            // outputXmlTextBox
            // 
            this.outputXmlTextBox.Location = new System.Drawing.Point(164, 43);
            this.outputXmlTextBox.Name = "outputXmlTextBox";
            this.outputXmlTextBox.ReadOnly = true;
            this.outputXmlTextBox.Size = new System.Drawing.Size(692, 20);
            this.outputXmlTextBox.TabIndex = 7;
            // 
            // treePanel
            // 
            this.treePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.treePanel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.treePanel.Controls.Add(this.xmlStatusLabel);
            this.treePanel.Controls.Add(this.treeBuilderStatusLevel);
            this.treePanel.Controls.Add(this.readingStatusLabel);
            this.treePanel.Controls.Add(this.directoryTreeLabel);
            this.treePanel.Controls.Add(this.directoryTreeView);
            this.treePanel.Location = new System.Drawing.Point(11, 105);
            this.treePanel.Name = "treePanel";
            this.treePanel.Size = new System.Drawing.Size(500, 490);
            this.treePanel.TabIndex = 8;
            // 
            // xmlStatusLabel
            // 
            this.xmlStatusLabel.AutoSize = true;
            this.xmlStatusLabel.Location = new System.Drawing.Point(316, 19);
            this.xmlStatusLabel.Name = "xmlStatusLabel";
            this.xmlStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.xmlStatusLabel.TabIndex = 7;
            // 
            // treeBuilderStatusLevel
            // 
            this.treeBuilderStatusLevel.AutoSize = true;
            this.treeBuilderStatusLevel.Location = new System.Drawing.Point(211, 18);
            this.treeBuilderStatusLevel.Name = "treeBuilderStatusLevel";
            this.treeBuilderStatusLevel.Size = new System.Drawing.Size(0, 13);
            this.treeBuilderStatusLevel.TabIndex = 6;
            // 
            // readingStatusLabel
            // 
            this.readingStatusLabel.AutoSize = true;
            this.readingStatusLabel.Location = new System.Drawing.Point(125, 19);
            this.readingStatusLabel.Name = "readingStatusLabel";
            this.readingStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.readingStatusLabel.TabIndex = 5;
            // 
            // directoryTreeLabel
            // 
            this.directoryTreeLabel.AutoSize = true;
            this.directoryTreeLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.directoryTreeLabel.Location = new System.Drawing.Point(6, 19);
            this.directoryTreeLabel.Name = "directoryTreeLabel";
            this.directoryTreeLabel.Size = new System.Drawing.Size(74, 13);
            this.directoryTreeLabel.TabIndex = 3;
            this.directoryTreeLabel.Text = "Directory Tree";
            // 
            // errorLogPanel
            // 
            this.errorLogPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorLogPanel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.errorLogPanel.Controls.Add(this.logListBox);
            this.errorLogPanel.Controls.Add(this.errorLogLabel);
            this.errorLogPanel.Location = new System.Drawing.Point(520, 105);
            this.errorLogPanel.Name = "errorLogPanel";
            this.errorLogPanel.Size = new System.Drawing.Size(336, 490);
            this.errorLogPanel.TabIndex = 10;
            // 
            // logListBox
            // 
            this.logListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logListBox.FormattingEnabled = true;
            this.logListBox.HorizontalScrollbar = true;
            this.logListBox.Location = new System.Drawing.Point(0, 44);
            this.logListBox.Name = "logListBox";
            this.logListBox.Size = new System.Drawing.Size(336, 446);
            this.logListBox.TabIndex = 11;
            // 
            // errorLogLabel
            // 
            this.errorLogLabel.AutoSize = true;
            this.errorLogLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.errorLogLabel.Location = new System.Drawing.Point(3, 19);
            this.errorLogLabel.Name = "errorLogLabel";
            this.errorLogLabel.Size = new System.Drawing.Size(55, 13);
            this.errorLogLabel.TabIndex = 10;
            this.errorLogLabel.Text = "Error Logs";
            // 
            // treeBuilderHeaderLabel
            // 
            this.treeBuilderHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.treeBuilderHeaderLabel.AutoSize = true;
            this.treeBuilderHeaderLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.treeBuilderHeaderLabel.Location = new System.Drawing.Point(205, 89);
            this.treeBuilderHeaderLabel.Name = "treeBuilderHeaderLabel";
            this.treeBuilderHeaderLabel.Size = new System.Drawing.Size(99, 13);
            this.treeBuilderHeaderLabel.TabIndex = 4;
            this.treeBuilderHeaderLabel.Text = "Tree building status";
            // 
            // ReadingHeaderLabel
            // 
            this.ReadingHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ReadingHeaderLabel.AutoSize = true;
            this.ReadingHeaderLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.ReadingHeaderLabel.Location = new System.Drawing.Point(119, 89);
            this.ReadingHeaderLabel.Name = "ReadingHeaderLabel";
            this.ReadingHeaderLabel.Size = new System.Drawing.Size(80, 13);
            this.ReadingHeaderLabel.TabIndex = 11;
            this.ReadingHeaderLabel.Text = "Reading Status";
            // 
            // xmlHeaderLabel
            // 
            this.xmlHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.xmlHeaderLabel.AutoSize = true;
            this.xmlHeaderLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.xmlHeaderLabel.Location = new System.Drawing.Point(310, 89);
            this.xmlHeaderLabel.Name = "xmlHeaderLabel";
            this.xmlHeaderLabel.Size = new System.Drawing.Size(102, 13);
            this.xmlHeaderLabel.TabIndex = 12;
            this.xmlHeaderLabel.Text = "XmlSerializing status";
            // 
            // DirectoryScannerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 607);
            this.Controls.Add(this.xmlHeaderLabel);
            this.Controls.Add(this.errorLogPanel);
            this.Controls.Add(this.treeBuilderHeaderLabel);
            this.Controls.Add(this.ReadingHeaderLabel);
            this.Controls.Add(this.treePanel);
            this.Controls.Add(this.outputXmlTextBox);
            this.Controls.Add(this.rootDirectoryTextBox);
            this.Controls.Add(this.processLabel);
            this.Controls.Add(this.outputButton);
            this.Controls.Add(this.pathButton);
            this.Controls.Add(this.startScanButton);
            this.Name = "DirectoryScannerForm";
            this.Text = "Directory Scanner Utility";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DirectoryTree_FormClosed);
            this.treePanel.ResumeLayout(false);
            this.treePanel.PerformLayout();
            this.errorLogPanel.ResumeLayout(false);
            this.errorLogPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button startScanButton;
        private System.Windows.Forms.TreeView directoryTreeView;
        private System.Windows.Forms.FolderBrowserDialog pathBrowsingDialog;
        private System.Windows.Forms.Button pathButton;
        private System.Windows.Forms.Button outputButton;
        private System.Windows.Forms.SaveFileDialog xmlOutputSaveFileDialog;
        private System.Windows.Forms.Label processLabel;
		private System.Windows.Forms.TextBox rootDirectoryTextBox;
		private System.Windows.Forms.TextBox outputXmlTextBox;
		private System.Windows.Forms.Panel treePanel;
		private System.Windows.Forms.Panel errorLogPanel;
		private System.Windows.Forms.Label directoryTreeLabel;
		private System.Windows.Forms.Label errorLogLabel;
        private System.Windows.Forms.ListBox logListBox;
        private System.Windows.Forms.Label treeBuilderHeaderLabel;
        private System.Windows.Forms.Label ReadingHeaderLabel;
        private System.Windows.Forms.Label xmlHeaderLabel;
        private System.Windows.Forms.Label xmlStatusLabel;
        private System.Windows.Forms.Label treeBuilderStatusLevel;
        private System.Windows.Forms.Label readingStatusLabel;
    }
}

