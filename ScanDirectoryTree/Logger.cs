﻿using System.Drawing;
using System.Windows.Forms;

namespace ScanDirectoryTree
{
    public class Logger
    {
        private ListBox _logbox;
        private Label _readStatus;
        private Label _drawStatus;
        private Label _saveStatus;

        public Logger(ListBox logbox, Label readStatus, Label drawStatus, Label saveStatus)
        {
            _logbox = logbox;
            _readStatus = readStatus;
            _drawStatus = drawStatus;
            _saveStatus = saveStatus;
        }
        public void Warning(string format, params object[] objectives)
        {
            format = string.Format("[Warn] {0}", format);
            writeLog(string.Format(format, objectives));
        }
        public void Error(string format, params object[] objectives)
        {
            format = string.Format("[Err] {0}", format);
            writeLog(string.Format(format, objectives));
        }

        private void writeLog(string message)
        {
            _logbox.Invoke((MethodInvoker)(() =>
            {
                _logbox.BeginUpdate();

                _logbox.Items.Add(message);

                _logbox.SelectedIndex = _logbox.Items.Count - 1;

                _logbox.EndUpdate();
            }));
        }

        public void UpdateReadStatus(string statusMessage, Color color)
        {
            updateLabel(_readStatus, statusMessage, color);
        }
        public void UpdateDrawStatus(string statusMessage, Color color)
        {
            updateLabel(_drawStatus, statusMessage, color);

        }
        public void UpdateSaveStatus(string statusMessage, Color color)
        {
            updateLabel(_saveStatus, statusMessage, color);

        }

        private void updateLabel(Label label, string statusMessage, Color color)
        {
            if (label.InvokeRequired)
            {
                label.Invoke((MethodInvoker)(() =>
                {
                    label.Text = statusMessage;
                    label.ForeColor = color;
                }));
            }
            else
            {
                label.Text = statusMessage;
                label.ForeColor = color;
            }
        }
    }
}
