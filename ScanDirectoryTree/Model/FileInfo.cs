﻿using ScanDirectoryTree.Win32.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;

namespace ScanDirectoryTree.Model
{
    public class FileInfo : InfoBase
    {
        #region properties for serialization
        public FileAttributes Attributes { get; set; }
        #endregion

        #region constructors
        public FileInfo() { }
        public FileInfo(string path)
        {
            this.Exceptions = new List<Exception>();
            this.Path = path;
            this.Name = System.IO.Path.GetFileName(this.Path);
            this.Attributes = LongPathFile.GetFileAttributes(path);
            this.CreatedOn = LongPathFile.CreatedOn(path);
            this.LastAccessedDate = LongPathFile.LastAccessDate(path);
            this.ModifiedOn = LongPathFile.ModifiedOn(path);
            this.Size = LongPathFile.Size(path);
            this.Type = InfoType.File;

            this.getAccessControl();

            try
            {
                this.Owner = File.GetAccessControl(path).GetOwner(typeof(NTAccount)).ToString();
            }
            catch (Exception e)
            {
                Exceptions.Add(e);
            }
        }
        #endregion

        private void getAccessControl()
        {
            try
            {
                var access = LongPathFile.GetAccessControl(this.Path, AccessControlSections.Owner | AccessControlSections.Access);
                var accessRules = access.GetAccessRules(true, true, typeof(NTAccount)).OfType<FileSystemAccessRule>().FirstOrDefault(rule => rule.IdentityReference.Value.StartsWith((string.Concat(Environment.MachineName, "\\", Environment.UserName)), StringComparison.InvariantCultureIgnoreCase));

                this.UserAccess = accessRules != null ? string.Intern(accessRules.FileSystemRights.ToString()) : string.Empty;
            }
            catch (Exception e)
            {
                this.Exceptions.Add(e);
            }
        }

    }
}
