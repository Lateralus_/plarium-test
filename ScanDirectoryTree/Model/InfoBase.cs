﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace ScanDirectoryTree.Model
{
    public enum InfoType
    {
        Directory = 0,
        File = 1
    } 
    public class InfoBase
    {
        [XmlIgnore]
        public List<Exception> Exceptions;

        #region properties for serialization
        [XmlAttribute]
        public Guid Id { get; set; }
        public InfoType Type { get; set; }
        public Guid ParentId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Owner { get; set; }
        public string UserAccess { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime LastAccessedDate { get; set; }
        public double Size { get; set; }


        #endregion

        #region constructors
        public InfoBase()
        {
            this.Id = Guid.NewGuid();
        }
        public InfoBase(string path)
        {
            this.Id = Guid.NewGuid();
            this.Name = System.IO.Path.GetFileName(path);
        }
        #endregion
    }
}
