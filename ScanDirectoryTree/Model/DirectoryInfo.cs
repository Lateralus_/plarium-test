﻿using ScanDirectoryTree.Win32.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;

namespace ScanDirectoryTree.Model
{
    public class DirectoryInfo : InfoBase
    {
        #region constructors
        public DirectoryInfo() { }
        public DirectoryInfo(string path)
        {
            this.Exceptions = new List<Exception>();
            this.Path = path;
            this.Type = InfoType.Directory;
            this.Name = System.IO.Path.GetFileName(path);
            this.CreatedOn = LongPathDirectory.CreatedOn(path);
            this.ModifiedOn = LongPathDirectory.ModifiedOn(path);
            this.LastAccessedDate = LongPathDirectory.LastAccessDate(path);

            getAccessControl();

            try
            {
                this.Owner = LongPathDirectory.GetAccessControl(path).GetOwner(typeof(NTAccount)).ToString();//File.GetAccessControl(path).GetOwner(typeof(NTAccount)).ToString();
            }
            catch (IdentityNotMappedException)
            {
                this.Owner = LongPathDirectory.GetAccessControl(path).GetOwner(typeof(SecurityIdentifier)).ToString();
            }
            catch (Exception e)
            {
                Exceptions.Add(e);
            }

        }
        #endregion

        private void getAccessControl()
        {
            try
            {
                var access = LongPathDirectory.GetAccessControl(this.Path, (AccessControlSections.Owner | AccessControlSections.Access));
                var accessRules = access.GetAccessRules(true, true, typeof(NTAccount)).OfType<FileSystemAccessRule>()
                    .FirstOrDefault(rule => rule.IdentityReference.Value.StartsWith((string.Concat(Environment.MachineName, "\\",
                    Environment.UserName)), StringComparison.InvariantCultureIgnoreCase));

                this.UserAccess = accessRules != null ? string.Intern(accessRules.FileSystemRights.ToString()) : string.Empty;
            }

            catch (Exception e)
            {
                this.Exceptions.Add(e);
            }
        }
    }
}
